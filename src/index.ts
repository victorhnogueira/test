const obj = [
  {
    name: "victor",
    age: 23,
  },
  {
    name: "hugo",
    age: 23,
  },
] as const;

export default obj;
